'use strict'

const fs = require('fs')

class ChatController {

    async answer({ request, response }) {

        if (!request.ajax()) {
            response.badRequest('forbidden')
            return
        }

        const question = request.get().q.toLowerCase()

        return this.responseMessage(question)

    }

    responseMessage(question) {
        const answer = "I'm sorry, I don't understand. Could you ask another question?"

        const json = fs.readFileSync('resources/txt/csvjson.json')
        const pairs = JSON.parse(json)
        const obj = pairs[0]

        let maxScore = 0
        let keyMaxScore = ''
        for (let [key, value] of Object.entries(obj)) {


            const list1 = question.split(" ")
            const list2 = key.toLowerCase().split(" ")

            const union = new Set(list1.concat(list2))
            const arrUnion = [...union]

            let l1 = []
            let l2 = []
            let sumL1 = 0
            let sumL2 = 0
            for (let i of arrUnion) {
                if (list1.includes(i)) {
                    l1.push(1)
                    sumL1 += 1
                } else l1.push(0)

                if (list2.includes(i)) {
                    l2.push(1)
                    sumL2 += 1
                } else l2.push(0)
            }

            let c = 0
            for (let i in l1) {
                c += l1[i] * l2[i]
            }

            const cosine = c / Math.pow(sumL1 * sumL2, 0.5)
            if (cosine > maxScore) {
                maxScore = cosine
                keyMaxScore = key
            }

        }

        if (maxScore < 0.4) return answer
        else return obj[keyMaxScore]
    }
}

module.exports = ChatController